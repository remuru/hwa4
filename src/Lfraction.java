import java.util.Objects;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class  Lfraction implements Comparable<Lfraction> {
   private long numerator;
   private long denominator;

   /** Main method. Different tests. */
   public static void main (String[] param) {
      // TODO!!! Your debugging tests here
   }

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b == 0)
         throw new ArithmeticException(
                 "Denominator cannot be 0");
      numerator = a;
      denominator = b;
      reduce();
   }

   //From https://www.geeksforgeeks.org/reduce-the-fraction-to-its-lowest-form/
   protected void reduce()
   {
      long d;
      d = gcd(numerator, denominator);

      numerator = numerator / d;
      denominator = denominator / d;
      if (denominator < 0) {
         numerator = numerator * -1;
         denominator = denominator * -1;
      }
   }

   static long gcd(long a, long b)
   {
      if (b == 0)
         return a;
      return gcd(b, a % b);

   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return numerator + "/" + denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      return compareTo((Lfraction) m) == 0;   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(denominator,numerator);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      Lfraction sum = new Lfraction(numerator * m.denominator + denominator
              * m.numerator, denominator * m.denominator);
      return sum;
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      Lfraction answer = new Lfraction(numerator * m.numerator, denominator
              * m.denominator);
      return answer;
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (numerator == 0)
         throw new ArithmeticException("Cannot inverse, numerator is 0");
      return new Lfraction(denominator, numerator);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(numerator, -1 * denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {

      Lfraction answer = plus(m.opposite());
      return answer;
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      return this.times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      Lfraction comparable = this.minus(m);
      if (comparable.numerator == 0)
         return 0;
      else if (comparable.numerator > 0)
         return 1;
      else
         return -1;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(numerator, denominator);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return numerator / denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return this.minus(new Lfraction(integerPart(), 1));   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double) numerator / (double) denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction((long) Math.round(f * d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      long a = 0;
      long b = 0;
      String[] parts = s.split("/");
      String part1 = parts[0]; // 004
      String part2 = parts[1];
      try {
         a = Long.parseLong(part1);
         b = Long.parseLong(part2);
         return new Lfraction(a,b);

      } catch (Exception e) {
         throw new RuntimeException("Can't convert to a fraction!");
      }
   }
}

